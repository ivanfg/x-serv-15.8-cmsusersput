from django.http import HttpResponse

from .models import Contenidos
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect

formulario = """
No existe valor en la base de datos para esta llave. 
<p>Introdúcela:
<p>
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""


@csrf_exempt
def get_content(request, llave):

    # Si es PUT:
    if request.method in ["POST", "PUT"]:
        if request.method == "POST":
            valor = request.POST['valor']#tomamos los datos del cuerpo de HTTP y los guardamos en la base de datos
        else:
            valor = request.body.decode('utf-8')
        try:
            c = Contenidos.objects.get(clave=llave)
            c.delete()
        except Contenidos.DoesNotExist:
            pass
        c = Contenidos(clave=llave, valor=valor)
        c.save()# lo guardo en la base de datos

    try: #GET
        contenidos = Contenidos.objects.get(clave=llave)
        respuesta = "El valor de la llave es: " + contenidos.valor
    except Contenidos.DoesNotExist:
        if request.user.is_authenticated:
            respuesta = formulario
        else:
            respuesta = "No estás autenticado. <a href='/login'>Haz login!</a> "
    return HttpResponse(respuesta)


def index(request):
    #1. Tener la lista de contenidos
    content_list = Contenidos.objects.all()
    #2. Cargar la plantilla
    template = loader.get_template('cms/index.html')
    #3. Ligar las variables de la plantilla a las variables de Python
    context = {
        'content_list': content_list
    }
    #4. "Renderizar"... es ejecutar 3.
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username
    else:
        respuesta = "No estás autenticado. <a href='/login'>Autentícate</a> "
    return HttpResponse(respuesta)


def logout_view(request):
    logout(request)
    return redirect("/cms/")

def imagen(request):
    template = loader.get_template('cms/plantilla.html')
    context = {}
    return HttpResponse(template.render(context, request))